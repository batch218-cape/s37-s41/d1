// Require model so we could use the model for searching
const User = require("../models/User.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const course = require("../models/course.js");

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email }).then(result => {
		// condition if there is an exsiting  user
		if(result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else
		{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
		// hashing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin ? reqBody.isAdmin : false
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}

module.exports.getProfile = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
}

// Enroll user to a class
module.exports.enrollUser = async (data, isAdmin) => {
	if(!isAdmin) {
		return false;
	}

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({
			courseId: data.courseId
		});

		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	});

	let isCourseUpdated = await course.findById(data.courseId).then(course => {
		course.enrollees.push({
			userId: data.userId
		});

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	});

	if (isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}
};
