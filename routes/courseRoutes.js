const express = require("express"); // accessing package of express
const router = express.Router(); // use dot notation to access content of a package

const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");

//create a single course
router.post("/create", auth.verify, (req, res) => {
    const newData = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseController.addCourse(req.body, newData).then(
        resultFromController => res.send(resultFromController)
    )
})

router.get("/all", (req, res) => {
    courseController.getAllCourse().then(
        resultFromController => res.send(resultFromController)
    )
})

router.get("/active", (req, res) => {
    courseController.getActiveCourses().then(
        resultFromController => res.send(resultFromController)
    )
})

router.get("/:courseId", (req, res) => {
                                // retrieves the id from the url
    courseController.getCourse(req.params.courseId).then(
        resultFromController => res.send(resultFromController)
    )
})

router.patch("/:courseId/update", auth.verify, (req, res) => {
    const newData = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseController.updateCourse(req.params.courseId, newData).then(
        resultFromController => {
            res.send(resultFromController)
        }
    )
})

router.patch("/:courseId/archive", auth.verify, (req, res) => {
    const newData = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseController.archiveCourse(req.params.courseId, newData).then(
        resultFromController => {
            res.send(resultFromController)
        }
    )
})

module.exports = router;
