// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/details/:id", (req, res) => {
	userController.getProfile(req.params.id).then(result => res.send(result))
});

router.post("/enroll", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	userController.enrollUser(req.body, isAdmin).then(resultFromController => res.send(resultFromController))
});

module.exports = router;